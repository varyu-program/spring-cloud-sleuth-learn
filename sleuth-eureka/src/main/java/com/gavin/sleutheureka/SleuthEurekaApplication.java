package com.gavin.sleutheureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication(scanBasePackages = "com.gavin.*")
public class SleuthEurekaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SleuthEurekaApplication.class, args);
    }

}
