package com.gavin.seluthconsumer.feigninterface;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//调用provider服务的feign客户端
@FeignClient("sleuth-provider")
public interface SleuthFeign {
    @GetMapping("/hello/{name}")
    public String sayHello(@PathVariable("name")String name);
}
