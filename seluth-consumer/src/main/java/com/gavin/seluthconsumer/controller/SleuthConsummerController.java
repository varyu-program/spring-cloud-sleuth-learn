package com.gavin.seluthconsumer.controller;

import com.gavin.seluthconsumer.feigninterface.SleuthFeign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@Slf4j
@RestController
public class SleuthConsummerController {

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private SleuthFeign sleuthFeign;

    @GetMapping("/hello/rest/{name}")
    public String testRestTemplate(@PathVariable("name") String name) {

        log.info("使用RestTemplate请求参数:{}", name);
        String url = "http://sleuth-provider/hello/{name}";
        log.info("使用RestTemplate请求url:{}", url);
        String result = restTemplate.getForObject(url, String.class, name);
        log.info("使用RestTemplate请求结果:{}", result);
        return result;
    }

    @GetMapping("/hello/feign/{name}")
    public String testFeign(@PathVariable("name") String name) {
        log.info("使用Feign请求参数:{}", name);
        String result = sleuthFeign.sayHello(name);
        log.info("使用Feign请求结果:{}", result);
        return result;
    }

}
