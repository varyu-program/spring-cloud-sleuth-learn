package com.gavin.seluthconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients(basePackages = "com.gavin.*")
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = "com.gavin.*")
public class SeluthConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SeluthConsumerApplication.class, args);
    }

}
