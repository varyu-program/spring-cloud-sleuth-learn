package com.gavin.seluthprovider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = "com.gavin.*")
public class SeluthProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(SeluthProviderApplication.class, args);
    }

}
