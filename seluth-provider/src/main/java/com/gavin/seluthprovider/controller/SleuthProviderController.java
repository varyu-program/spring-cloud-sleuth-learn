package com.gavin.seluthprovider.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class SleuthProviderController {

    @GetMapping("/hello/{name}")
    public String sayHello(@PathVariable("name") String name) {
        log.info("请求参数:{}", name);
        String result = "hello" + name;
        log.info("请求结果:{}", result);
        return result;
    }
}
